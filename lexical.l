%{
#include "syst.tab.h"
extern nb_ligne;
extern YYSTYPE yylval;
#include "fonctions.h"

%}


lettres [a-zA-Z]
chiffres [+-]?[0-9]+
cnt {chiffres}+
valeur_reel "-"?[0-9]+"."?[0-9]*
idf "$"{lettres}({lettres}|{chiffres}|{lettres}_)*
idfTab "@"{lettres}({lettres}|{chiffres}|{lettres}_)*
comment "##"([^#]|\n)*"##"


%%
import return mc_import;
ISIL.io {
            AjouterLib("ISIL.io");
            return bib_io;
        }
ISIL.lang {
            AjouterLib("ISIL.lang");
            return bib_lang;
          }
public return mc_public;
private return mc_private;
protected return mc_protected;
class return mc_class;


Entier  {    
            yylval.str=strdup("Entier");
            return mc_entier;
        }

Reel    {
            yylval.str=strdup("Reel");
            return  mc_reel;
        }

Chaine  {
            yylval.str=strdup("Chaine");
            return mc_chaine;
        }


{idfTab}    {
                yylval.str=strdup(yytext);
                return idf_tab;
			}


{idf}   {
            if (yyleng<20) {
                            yylval.str=strdup(yytext);
                            return idf;
						}
            else printf("erreur lexical a la ligne %d: l'idf %s trop long\n",nb_ligne,yytext);
	    }

{cnt}   {
            yylval.entier=atoi(yytext);
            return constint;
        }

{valeur_reel} return mc_valeur_reel;
CONST return mc_const; 
"{" return aco_ouv;
"}" return aco_frm;
"(" return arc_ouv;
")" return arc_frm;
"[" return cro_ouv;
"]" return cro_frm;
:= return egale;
; return pvg;
"," return vrg;

"+" {   
        yylval.str=strdup(yytext);
        return plus ;
    }

"-" {   
        yylval.str=strdup(yytext);
        return moin ;
    } 

"*" {   
        yylval.str=strdup(yytext);
        return multi;
    }

"/" {  
        yylval.str=strdup(yytext);
        return slach;
    }

"<" return inf;
> return sup;
! return diff;

for return bou_for;
main return mc_main;
In return in_instru;
Out return output;

\"[^"]*\" {
            yylval.str=strdup(yytext);
            return chaine_const;
        }

{comment} { yylval.str=strdup(yytext);
            return commentaire;
        }  

[ \t]
\n nb_ligne++;
. printf("erreur lexicale a la ligne %d avec l'entite %s\n",nb_ligne,yytext);



