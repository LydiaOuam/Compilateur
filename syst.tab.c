
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C
   
      Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.4.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Copy the first part of user declarations.  */

/* Line 189 of yacc.c  */
#line 9 "syst.y"

int nb_ligne= 1;
char sauvType[20];

char souvop;


/* Line 189 of yacc.c  */
#line 81 "syst.tab.c"

/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     mc_import = 258,
     bib_io = 259,
     bib_lang = 260,
     pvg = 261,
     aco_frm = 262,
     aco_ouv = 263,
     idf = 264,
     mc_class = 265,
     mc_public = 266,
     mc_entier = 267,
     mc_private = 268,
     mc_protected = 269,
     mc_reel = 270,
     mc_chaine = 271,
     idf_tab = 272,
     constint = 273,
     cro_ouv = 274,
     cro_frm = 275,
     mc_const = 276,
     vrg = 277,
     egale = 278,
     chaine_const = 279,
     mc_valeur_reel = 280,
     bou_for = 281,
     arc_ouv = 282,
     arc_frm = 283,
     moin = 284,
     plus = 285,
     inf = 286,
     sup = 287,
     diff = 288,
     multi = 289,
     slach = 290,
     in_instru = 291,
     output = 292,
     mc_main = 293,
     commentaire = 294,
     guillemets = 295
   };
#endif



#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 214 of yacc.c  */
#line 15 "syst.y"


int  entier;
char* str;
float reel;




/* Line 214 of yacc.c  */
#line 167 "syst.tab.c"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif


/* Copy the second part of user declarations.  */


/* Line 264 of yacc.c  */
#line 179 "syst.tab.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int yyi)
#else
static int
YYID (yyi)
    int yyi;
#endif
{
  return yyi;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)				\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack_alloc, Stack, yysize);			\
	Stack = &yyptr->Stack_alloc;					\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  8
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   136

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  41
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  31
/* YYNRULES -- Number of rules.  */
#define YYNRULES  70
/* YYNRULES -- Number of states.  */
#define YYNSTATES  149

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   295

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint8 yyprhs[] =
{
       0,     0,     3,     6,     9,    10,    14,    16,    18,    25,
      27,    29,    31,    34,    41,    44,    45,    47,    49,    51,
      53,    55,    60,    68,    74,    75,    77,    79,    81,    83,
      85,    89,    91,    98,   103,   105,   107,   109,   114,   116,
     121,   125,   127,   129,   134,   136,   138,   140,   142,   144,
     146,   148,   160,   164,   168,   175,   177,   179,   182,   185,
     188,   192,   196,   200,   204,   208,   215,   222,   226,   227,
     235
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int8 yyrhs[] =
{
      42,     0,    -1,    43,    46,    -1,    44,    43,    -1,    -1,
       3,    45,     6,    -1,     4,    -1,     5,    -1,    47,    10,
       9,     8,    48,     7,    -1,    11,    -1,    13,    -1,    14,
      -1,    52,    49,    -1,    38,    27,    28,     8,    50,     7,
      -1,    51,    50,    -1,    -1,    63,    -1,    68,    -1,    69,
      -1,    39,    -1,    58,    -1,    57,    53,     6,    52,    -1,
      21,    57,     9,    23,    54,     6,    52,    -1,    21,    57,
       9,     6,    52,    -1,    -1,    55,    -1,    56,    -1,    18,
      -1,    24,    -1,    25,    -1,     9,    22,    55,    -1,     9,
      -1,    17,    19,    18,    20,    22,    56,    -1,    17,    19,
      18,    20,    -1,    12,    -1,    15,    -1,    16,    -1,    59,
      23,    60,     6,    -1,     9,    -1,    17,    19,    18,    20,
      -1,    61,    62,    60,    -1,    61,    -1,     9,    -1,    17,
      19,    18,    20,    -1,    24,    -1,    18,    -1,    25,    -1,
      30,    -1,    29,    -1,    34,    -1,    35,    -1,    26,    27,
      64,     6,    66,     6,    67,    28,     8,    50,     7,    -1,
       9,    23,    18,    -1,     9,    23,     9,    -1,     9,    23,
      17,    19,    18,    20,    -1,    31,    -1,    32,    -1,    33,
      23,    -1,    23,    31,    -1,    23,    32,    -1,     9,    65,
       9,    -1,     9,    65,    18,    -1,    18,    65,     9,    -1,
       9,    30,    30,    -1,     9,    29,    29,    -1,    37,    27,
      24,    70,    28,     6,    -1,    36,    27,    24,    70,    28,
       6,    -1,    22,     9,    70,    -1,    -1,    22,    17,    19,
      18,    20,    71,    70,    -1,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint8 yyrline[] =
{
       0,    23,    23,    26,    27,    29,    31,    32,    35,    38,
      38,    38,    41,    44,    47,    48,    51,    52,    53,    54,
      58,    62,    63,    70,    75,    78,    78,    80,    80,    80,
      82,    89,    96,   109,   124,   125,   126,   137,   143,   151,
     160,   161,   164,   165,   171,   172,   174,   178,   179,   180,
     181,   188,   192,   193,   194,   200,   200,   200,   200,   200,
     203,   204,   205,   208,   209,   219,   225,   232,   236,   236,
     240
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "mc_import", "bib_io", "bib_lang", "pvg",
  "aco_frm", "aco_ouv", "idf", "mc_class", "mc_public", "mc_entier",
  "mc_private", "mc_protected", "mc_reel", "mc_chaine", "idf_tab",
  "constint", "cro_ouv", "cro_frm", "mc_const", "vrg", "egale",
  "chaine_const", "mc_valeur_reel", "bou_for", "arc_ouv", "arc_frm",
  "moin", "plus", "inf", "sup", "diff", "multi", "slach", "in_instru",
  "output", "mc_main", "commentaire", "guillemets", "$accept", "S",
  "LISTE_BIB", "BIB", "Nom_BIB", "HEADER_CLASS", "MODIFICATEUR", "CORPS",
  "Partie_Main", "Partie_INST", "INST", "Partie_DEC_VAR", "liste_idfs",
  "var", "list_var", "list_tab", "TYPE", "Instru_affect", "type_idf",
  "Liste_affect", "var_affect", "Operation", "Boucle_for", "IntFor",
  "Compare", "Condition", "Increment", "Ecrire", "Lecture", "liste_io",
  "$@1", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    41,    42,    43,    43,    44,    45,    45,    46,    47,
      47,    47,    48,    49,    50,    50,    51,    51,    51,    51,
      51,    52,    52,    52,    52,    53,    53,    54,    54,    54,
      55,    55,    56,    56,    57,    57,    57,    58,    59,    59,
      60,    60,    61,    61,    61,    61,    61,    62,    62,    62,
      62,    63,    64,    64,    64,    65,    65,    65,    65,    65,
      66,    66,    66,    67,    67,    68,    69,    70,    71,    70,
      70
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     2,     2,     0,     3,     1,     1,     6,     1,
       1,     1,     2,     6,     2,     0,     1,     1,     1,     1,
       1,     4,     7,     5,     0,     1,     1,     1,     1,     1,
       3,     1,     6,     4,     1,     1,     1,     4,     1,     4,
       3,     1,     1,     4,     1,     1,     1,     1,     1,     1,
       1,    11,     3,     3,     6,     1,     1,     2,     2,     2,
       3,     3,     3,     3,     3,     6,     6,     3,     0,     7,
       0
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       4,     0,     0,     0,     4,     6,     7,     0,     1,     9,
      10,    11,     2,     0,     3,     5,     0,     0,    24,    34,
      35,    36,     0,     0,     0,     0,     0,     8,     0,    12,
      31,     0,     0,    25,    26,     0,     0,     0,     0,    24,
      24,     0,     0,    30,     0,    21,    23,    27,    28,    29,
       0,    15,    33,    24,    38,     0,     0,     0,     0,    19,
       0,    15,    20,     0,    16,    17,    18,     0,    22,     0,
       0,     0,     0,    13,    14,     0,    32,     0,     0,     0,
      70,    70,    42,     0,    45,    44,    46,     0,    41,    39,
       0,     0,     0,     0,     0,     0,    37,    48,    47,    49,
      50,     0,    53,     0,    52,     0,     0,     0,    70,     0,
       0,     0,     0,    40,     0,     0,    55,    56,     0,     0,
       0,     0,    67,     0,    66,    65,    43,     0,    58,    59,
      57,    60,    61,    62,     0,     0,     0,    54,     0,     0,
       0,    68,    64,    63,    15,    70,     0,    69,    51
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     2,     3,     4,     7,    12,    13,    23,    29,    60,
      61,    24,    32,    50,    33,    34,    25,    62,    63,    87,
      88,   101,    64,    79,   119,   107,   135,    65,    66,    93,
     145
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -81
static const yytype_int8 yypact[] =
{
       8,    53,     6,    42,     8,   -81,   -81,    18,   -81,   -81,
     -81,   -81,   -81,    34,   -81,   -81,    45,    55,    19,   -81,
     -81,   -81,    36,    57,    -1,    29,    58,   -81,    39,   -81,
      46,    50,    64,   -81,   -81,    -3,    43,    63,    56,    19,
      19,    25,    65,   -81,    59,   -81,   -81,   -81,   -81,   -81,
      69,    -7,    54,    19,   -81,    61,    51,    60,    62,   -81,
      70,    -7,   -81,    67,   -81,   -81,   -81,    68,   -81,    66,
      72,    71,    73,   -81,   -81,    -2,   -81,    74,    75,    76,
      77,    77,   -81,    81,   -81,   -81,   -81,    80,   -21,   -81,
      24,     3,    30,    78,    79,    83,   -81,   -81,   -81,   -81,
     -81,    -2,   -81,    84,   -81,    -6,    -6,    82,    77,    85,
      86,    87,    88,   -81,    91,    28,   -81,   -81,    89,    27,
      93,    96,   -81,    92,   -81,   -81,   -81,    94,   -81,   -81,
     -81,   -81,   -81,   -81,    32,    90,    95,   -81,    97,    98,
     103,   -81,   -81,   -81,    -7,    77,   106,   -81,   -81
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -81,   -81,   112,   -81,   -81,   -81,   -81,   -81,   -81,   -61,
     -81,   -35,   -81,   -81,    99,    52,   100,   -81,   -81,   -10,
     -81,   -81,   -81,   -81,    11,   -81,   -81,   -81,   -81,   -80,
     -81
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -1
static const yytype_uint8 yytable[] =
{
      74,    94,    54,    40,    45,    46,     8,    82,    97,    98,
      55,     1,   105,    99,   100,    83,    84,   115,    68,    56,
      41,   106,    85,    86,    15,   116,   117,   118,   122,    57,
      58,    19,    59,   102,    20,    21,   131,    28,    30,   108,
      22,   103,   104,    47,    16,   132,    31,   109,    19,    48,
      49,    20,    21,     9,    17,    10,    11,     5,     6,   128,
     129,   138,   139,    18,    27,   147,    36,    35,    37,    38,
      39,    42,    30,    51,    44,    53,    67,    73,    70,    52,
      69,    78,    91,   146,    77,    31,    96,    71,   121,    72,
      75,   113,   124,   125,    89,    80,     0,    81,    90,    92,
      95,   112,   133,   114,   123,   134,   110,   111,   126,   127,
     136,   144,   130,   148,   137,   141,    14,   120,   140,    76,
       0,     0,    26,     0,     0,     0,   142,     0,   143,     0,
       0,     0,     0,     0,     0,     0,    43
};

static const yytype_int16 yycheck[] =
{
      61,    81,     9,     6,    39,    40,     0,     9,    29,    30,
      17,     3,     9,    34,    35,    17,    18,    23,    53,    26,
      23,    18,    24,    25,     6,    31,    32,    33,   108,    36,
      37,    12,    39,     9,    15,    16,     9,    38,     9,     9,
      21,    17,    18,    18,    10,    18,    17,    17,    12,    24,
      25,    15,    16,    11,     9,    13,    14,     4,     5,    31,
      32,    29,    30,     8,     7,   145,    27,     9,    22,    19,
       6,    28,     9,     8,    18,     6,    22,     7,    27,    20,
      19,     9,     6,   144,    18,    17,     6,    27,     6,    27,
      23,   101,     6,     6,    20,    24,    -1,    24,    23,    22,
      19,    18,     9,    19,    19,     9,    28,    28,    20,    18,
      18,     8,    23,     7,    20,    20,     4,   106,    28,    67,
      -1,    -1,    22,    -1,    -1,    -1,    29,    -1,    30,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    37
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     3,    42,    43,    44,     4,     5,    45,     0,    11,
      13,    14,    46,    47,    43,     6,    10,     9,     8,    12,
      15,    16,    21,    48,    52,    57,    57,     7,    38,    49,
       9,    17,    53,    55,    56,     9,    27,    22,    19,     6,
       6,    23,    28,    55,    18,    52,    52,    18,    24,    25,
      54,     8,    20,     6,     9,    17,    26,    36,    37,    39,
      50,    51,    58,    59,    63,    68,    69,    22,    52,    19,
      27,    27,    27,     7,    50,    23,    56,    18,     9,    64,
      24,    24,     9,    17,    18,    24,    25,    60,    61,    20,
      23,     6,    22,    70,    70,    19,     6,    29,    30,    34,
      35,    62,     9,    17,    18,     9,    18,    66,     9,    17,
      28,    28,    18,    60,    19,    23,    31,    32,    33,    65,
      65,     6,    70,    19,     6,     6,    20,    18,    31,    32,
      23,     9,    18,     9,     9,    67,    18,    20,    29,    30,
      28,    20,    29,    30,     8,    71,    50,    70,     7
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
#else
static void
yy_stack_print (yybottom, yytop)
    yytype_int16 *yybottom;
    yytype_int16 *yytop;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}

/* Prevent warnings from -Wmissing-prototypes.  */
#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */


/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*-------------------------.
| yyparse or yypush_parse.  |
`-------------------------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{


    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       `yyss': related to states.
       `yyvs': related to semantic values.

       Refer to the stacks thru separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yytoken = 0;
  yyss = yyssa;
  yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */
  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss_alloc, yyss);
	YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:

/* Line 1455 of yacc.c  */
#line 23 "syst.y"
    {printf("syntaxe correcte !!");;}
    break;

  case 19:

/* Line 1455 of yacc.c  */
#line 54 "syst.y"
    {      
                            //printf("%s \n",$1);      
                            nb_ligne = nb_ligne+Nbr_ligneCom((yyvsp[(1) - (1)].str));
                     ;}
    break;

  case 22:

/* Line 1455 of yacc.c  */
#line 63 "syst.y"
    { 
               													
                                                               if(doubleDeclaration((yyvsp[(3) - (7)].str))==0){AjouterConstEx((yyvsp[(3) - (7)].str),sauvType);}   
                                                               else{printf("%d: Double declaration  de %s a la ligne \n",nb_ligne,(yyvsp[(3) - (7)].str));}
                                                            
                                                                             
                                                               ;}
    break;

  case 23:

/* Line 1455 of yacc.c  */
#line 70 "syst.y"
    { 
                                                               if(doubleDeclaration((yyvsp[(3) - (5)].str))==0){AjouterConstNex((yyvsp[(3) - (5)].str),sauvType);}   
                                                               else{printf("%d: Double declaration  de %s a la ligne \n",nb_ligne,(yyvsp[(3) - (5)].str));}
                                                                             
                                                               ;}
    break;

  case 30:

/* Line 1455 of yacc.c  */
#line 82 "syst.y"
    {
                            if(doubleDeclaration((yyvsp[(1) - (3)].str))==0)
                                     AjouterVar((yyvsp[(1) - (3)].str),sauvType);
                                     
                            else
		                     printf("%d: Double declaration  de %s a la ligne \n",nb_ligne,(yyvsp[(1) - (3)].str));
		              ;}
    break;

  case 31:

/* Line 1455 of yacc.c  */
#line 89 "syst.y"
    {if(doubleDeclaration((yyvsp[(1) - (1)].str))==0)
                     AjouterVar((yyvsp[(1) - (1)].str),sauvType);
              else
		        printf("%d: Double declaration  de %s a la ligne \n",nb_ligne,(yyvsp[(1) - (1)].str));
		;}
    break;

  case 32:

/* Line 1455 of yacc.c  */
#line 96 "syst.y"
    {
                                                               if(doubleDeclaration((yyvsp[(1) - (6)].str))==0){
                                                                      if((yyvsp[(3) - (6)].entier)<0){
                                                                             printf("taille du tableau doit etre plus de 0 \n");
                                                                      }
                                                                      else{
                                                                             AjouterTab((yyvsp[(1) - (6)].str),sauvType,(yyvsp[(3) - (6)].entier));  
                                                                      }                               
                                                               }
                                                               else{
                                                                       printf("%d: Double declaration  de %s a la ligne \n",nb_ligne,(yyvsp[(1) - (6)].str));
                                                               }          
                                                        ;}
    break;

  case 33:

/* Line 1455 of yacc.c  */
#line 109 "syst.y"
    {
                                                 if(doubleDeclaration((yyvsp[(1) - (4)].str))==0){
                                                        if((yyvsp[(3) - (4)].entier)<0){
                                                               printf("taille du tableau doit etre plus de 0 \n");
                                                        }
                                                        else{
                                                               AjouterTab((yyvsp[(1) - (4)].str),sauvType,(yyvsp[(3) - (4)].entier));  
                                                        }                               
                                                 }
                                                 else{
                                                         printf("%d: Double declaration  de %s a la ligne \n",nb_ligne,(yyvsp[(1) - (4)].str));
                                                 }
                                          ;}
    break;

  case 34:

/* Line 1455 of yacc.c  */
#line 124 "syst.y"
    {strcpy(sauvType,(yyvsp[(1) - (1)].str));;}
    break;

  case 35:

/* Line 1455 of yacc.c  */
#line 125 "syst.y"
    {strcpy(sauvType,(yyvsp[(1) - (1)].str));;}
    break;

  case 36:

/* Line 1455 of yacc.c  */
#line 126 "syst.y"
    {strcpy(sauvType,(yyvsp[(1) - (1)].str));;}
    break;

  case 37:

/* Line 1455 of yacc.c  */
#line 137 "syst.y"
    { 
                                                        if(Recherche("ISIL.lang")==-1){printf("Bibliotheque manquante! \n");}
                                                        else{if(Verifier()==0){printf("%d:Non compatibilite de type \n",nb_ligne);}} 
                                                 ;}
    break;

  case 38:

/* Line 1455 of yacc.c  */
#line 143 "syst.y"
    {
                           if(Recherche((yyvsp[(1) - (1)].str))==-1){printf("%d: IDF %s non declare \n",nb_ligne,(yyvsp[(1) - (1)].str));}
                           else{
                                   if(isConst((yyvsp[(1) - (1)].str))==1){printf("%d: impossible de changer le contenu d'une Const \n",nb_ligne);}
                                   else setvalConst((yyvsp[(1) - (1)].str));
                                   setTypeVarAffct((yyvsp[(1) - (1)].str));
                            }
                   ;}
    break;

  case 39:

/* Line 1455 of yacc.c  */
#line 151 "syst.y"
    {      
                                          if(VerfierDeppas((yyvsp[(1) - (4)].str),(yyvsp[(3) - (4)].entier))==0){printf("%d:Depassemnt de taille de tableau \n",nb_ligne);}
                                          else{
                                                 if(Recherche((yyvsp[(1) - (4)].str))==-1){printf("%d: IDF %s non declare \n",nb_ligne,(yyvsp[(1) - (4)].str));}
                                                 setTypeVarAffct((yyvsp[(1) - (4)].str));
                                          }
                                               
                                   ;}
    break;

  case 42:

/* Line 1455 of yacc.c  */
#line 164 "syst.y"
    {if(Recherche((yyvsp[(1) - (1)].str))==-1){printf("%d: IDF %s non declare \n",nb_ligne,(yyvsp[(1) - (1)].str));} AjouterOperand((yyvsp[(1) - (1)].str));;}
    break;

  case 43:

/* Line 1455 of yacc.c  */
#line 165 "syst.y"
    {
                                                        if(VerfierDeppas((yyvsp[(1) - (4)].str),(yyvsp[(3) - (4)].entier))==0){printf("%d:Depassemnt de taille de tableau \n",nb_ligne);}
                                                        if(Recherche((yyvsp[(1) - (4)].str))==-1){printf("%d: IDF %s non declare \n",nb_ligne,(yyvsp[(1) - (4)].str));}
                                                        else { AjouterOperand((yyvsp[(1) - (4)].str));}
                                                 ;}
    break;

  case 44:

/* Line 1455 of yacc.c  */
#line 171 "syst.y"
    {if(verfierCompa("Chaine")==0){printf("%d:Non compatibilite de type \n",nb_ligne);};}
    break;

  case 45:

/* Line 1455 of yacc.c  */
#line 172 "syst.y"
    {if(verfierCompa("Chaine")!=0){printf("%d:Non compatibilite de type \n",nb_ligne);} 
                            if(souvop=='/' && (yyvsp[(1) - (1)].entier)==0){printf("%d:erreur semantique division par 0 \n",nb_ligne);} ;}
    break;

  case 46:

/* Line 1455 of yacc.c  */
#line 174 "syst.y"
    {if(verfierCompa("Chaine")!=0){printf("%d:Incompatibilite de type\n",nb_ligne);} 
                            if(souvop=='/' && (yyvsp[(1) - (1)].entier)==0){printf("%d:erreur semantique division par 0 \n",nb_ligne);} ;}
    break;

  case 47:

/* Line 1455 of yacc.c  */
#line 178 "syst.y"
    {AjouterOperateur('+');;}
    break;

  case 48:

/* Line 1455 of yacc.c  */
#line 179 "syst.y"
    {AjouterOperateur('-');;}
    break;

  case 49:

/* Line 1455 of yacc.c  */
#line 180 "syst.y"
    {AjouterOperateur('*');;}
    break;

  case 50:

/* Line 1455 of yacc.c  */
#line 181 "syst.y"
    {AjouterOperateur('/'); souvop = '/';;}
    break;

  case 52:

/* Line 1455 of yacc.c  */
#line 192 "syst.y"
    {if(Recherche((yyvsp[(1) - (3)].str))==-1){printf("%d: IDF %s non declare \n",nb_ligne,(yyvsp[(1) - (3)].str));};}
    break;

  case 53:

/* Line 1455 of yacc.c  */
#line 193 "syst.y"
    {if(Recherche((yyvsp[(1) - (3)].str))==-1){printf("%d: IDF %s non declare \n",nb_ligne,(yyvsp[(1) - (3)].str));};}
    break;

  case 54:

/* Line 1455 of yacc.c  */
#line 194 "syst.y"
    {
                                                        if(Recherche((yyvsp[(1) - (6)].str))==-1){printf("%d: IDF %s non declare \n",nb_ligne,(yyvsp[(1) - (6)].str));}
                                                        if(VerfierDeppas((yyvsp[(3) - (6)].str),(yyvsp[(5) - (6)].entier))==0){printf("%d:Depassemnt de taille  \n",nb_ligne);}
                                                        ;}
    break;

  case 60:

/* Line 1455 of yacc.c  */
#line 203 "syst.y"
    {if((Recherche((yyvsp[(1) - (3)].str))==-1)&&(Recherche((yyvsp[(3) - (3)].str))==-1)){printf("%d: IDF %s non declare \n",nb_ligne,(yyvsp[(1) - (3)].str));};}
    break;

  case 61:

/* Line 1455 of yacc.c  */
#line 204 "syst.y"
    {if(Recherche((yyvsp[(1) - (3)].str))==-1){printf("%d: IDF %s non declare \n",nb_ligne,(yyvsp[(1) - (3)].str));};}
    break;

  case 62:

/* Line 1455 of yacc.c  */
#line 205 "syst.y"
    {if(Recherche((yyvsp[(3) - (3)].str))==-1){printf("%d: IDF %s non declare \n",nb_ligne,(yyvsp[(1) - (3)].entier));};}
    break;

  case 63:

/* Line 1455 of yacc.c  */
#line 208 "syst.y"
    {if(Recherche((yyvsp[(1) - (3)].str))==-1){printf("%d: IDF %s non declare \n",nb_ligne,(yyvsp[(1) - (3)].str));};}
    break;

  case 64:

/* Line 1455 of yacc.c  */
#line 209 "syst.y"
    {if(Recherche((yyvsp[(1) - (3)].str))==-1){printf("%d: IDF %s non declare \n",nb_ligne,(yyvsp[(1) - (3)].str));};}
    break;

  case 65:

/* Line 1455 of yacc.c  */
#line 219 "syst.y"
    {
                                                               if(Recherche("ISIL.io")==-1){printf("Bibliotheque manquante! \n");}
                                                               if(VerfierInOut((yyvsp[(3) - (6)].entier))==0){printf("%d :Erreur format non respete de la fonction Out ligne \n",nb_ligne);}
                                                         ;}
    break;

  case 66:

/* Line 1455 of yacc.c  */
#line 225 "syst.y"
    {
                                                                      if(Recherche("ISIL.io")==-1){printf("Bibliotheque manquante! \n");}
                                                                      if(VerfierInOut((yyvsp[(3) - (6)].entier))==0){printf("%d :Erreur format non respete de la fonction In ligne  \n",nb_ligne);}
                                                              ;}
    break;

  case 67:

/* Line 1455 of yacc.c  */
#line 232 "syst.y"
    {if(Recherche((yyvsp[(2) - (3)].str))==-1){printf("%d: IDF %s non declare \n",nb_ligne,(yyvsp[(2) - (3)].str));}
                     else{VarAffichage((yyvsp[(2) - (3)].str));}
                   ;}
    break;

  case 68:

/* Line 1455 of yacc.c  */
#line 236 "syst.y"
    {if(Recherche((yyvsp[(2) - (5)].str))==-1){printf("%d: IDF %s non declare \n",nb_ligne,(yyvsp[(2) - (5)].str));}
                                                  else {if(VerfierDeppas((yyvsp[(2) - (5)].str),(yyvsp[(4) - (5)].entier))==0){printf("%d:Depassemnt de taille  \n",nb_ligne);}  
                                                        else{VarAffichage((yyvsp[(2) - (5)].str));}}
                                                 ;}
    break;



/* Line 1455 of yacc.c  */
#line 1829 "syst.tab.c"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (yymsg);
	  }
	else
	  {
	    yyerror (YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined(yyoverflow) || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}



/* Line 1675 of yacc.c  */
#line 243 "syst.y"


main()
{yyparse();
Affichage();
}
yywrap()
{}
yyerror(char*msg)
{
printf("erreur syntaxique a la ligne %d\n",nb_ligne);
}

