

typedef struct 
{
    char NomEntite[20];
    char CodeEntite[20];
    char TypeEntite[20];
    char ValConstan[20];
    int TAILLETAB;
}TypeET;

TypeET TS[100];
int CP_TS = 0;

char VarOut[100],VarOut1[100];
int index_par = -1;

char TypeVarAffct[100];

char tabOperand[100][100];
int  index_taboperand = -1;

char tabOperateur[100];
int  index_taboperateur = -1;

int Recherche(char Entite[]){ //return l'indice de l'element si existe -1 sinon

		int i=0;
		while(i<CP_TS)
		{
            if (strcmp(Entite,TS[i].NomEntite)==0) return i;
            i++;
        }

		return -1;
}



void Ajouter(char Entite[],char Code[]){
    if ( Recherche(Entite)==-1)
        {
            strcpy(TS[CP_TS].NomEntite,Entite); 
            strcpy(TS[CP_TS].CodeEntite,Code);
            CP_TS++;
        }
}




void Affichage()
{
    printf("\n/***************Table des symboles ******************/\n");
    printf("____________________________________________________________________________\n");
    printf("| NomEntite |  CodeEntite | TyepEntite  | TailleTAB     | Valeur Constante |\n");
    printf("____________________________________________________________________________\n");
    int i=0;
    while(i<CP_TS)
    {
        printf("|%10s |%12s | %12s | %12d|%12s |\n",TS[i].NomEntite,TS[i].CodeEntite,TS[i].TypeEntite,TS[i].TAILLETAB,TS[i].ValConstan);
        i++;
    }
}

void AjouterType(char Entite[],char TYPE[]){
    int pos;
	   pos=Recherche(Entite);
	if(pos!=-1)
	   strcpy(TS[pos].TypeEntite,TYPE);
}

int doubleDeclaration(char Entite[])
	{
	int pos;
	pos=Recherche(Entite);
	if(strcmp(TS[pos].TypeEntite,"")==0) return 0;
	   else return -1;
}
//ajoouter les variables
void AjouterVar(char Entite[],char TYPE[]){
    if ( Recherche(Entite)==-1)
        {
            strcpy(TS[CP_TS].NomEntite,Entite); 
            strcpy(TS[CP_TS].CodeEntite,"VAR");
            strcpy(TS[CP_TS].TypeEntite,TYPE);
            CP_TS++;
        }

}
//ajouter les constantes
void AjouterConstEx(char Entite[],char TYPE[]){
    if ( Recherche(Entite)==-1)
        {
            strcpy(TS[CP_TS].NomEntite,Entite); 
            strcpy(TS[CP_TS].CodeEntite,"CONST");
            strcpy(TS[CP_TS].TypeEntite,TYPE);
            strcpy(TS[CP_TS].ValConstan,"1");
            CP_TS++;
        }

}
//ajouter les constantes
void AjouterConstNex(char Entite[],char TYPE[]){
    if ( Recherche(Entite)==-1)
        {
            strcpy(TS[CP_TS].NomEntite,Entite); 
            strcpy(TS[CP_TS].CodeEntite,"CONST");
            strcpy(TS[CP_TS].TypeEntite,TYPE);
            strcpy(TS[CP_TS].ValConstan,"0");
            CP_TS++;
        }

}
//ajouter un tableau et sa taille
void AjouterTab(char Entite[],char TYPE[],int TAILLE){
    if ( Recherche(Entite)==-1)
        {
            strcpy(TS[CP_TS].NomEntite,Entite); 
            strcpy(TS[CP_TS].CodeEntite,"TAB");
            strcpy(TS[CP_TS].TypeEntite,TYPE);
            TS[CP_TS].TAILLETAB = TAILLE;
            CP_TS++;
        }

}
//changer valeurconst
void setvalConst(char Entite[])
{
	int pos;
	  pos=Recherche(Entite);
	  if(pos != -1 && strcmp(TS[pos].CodeEntite,"CONST")==0)
	 	strcpy(TS[pos].ValConstan,"1");
	 
}
//verifier si la taille du tableau est respectee 1 si oui 0 sinon
int VerfierDeppas(char IDF[],int INDICE){
    int pos;
	pos=Recherche(IDF);
    if(TS[pos].TAILLETAB>INDICE && INDICE>=0){
        return 1;
    }
    return 0;
}

// verifier si l'idf correspond a une constante 1 si oui 0 sinon
int isConst(char IDF[]){
    int pos;
	pos=Recherche(IDF);
    //TS[pos].TAILLETAB>INDICE ? 1 : 0;
    if(strcmp(TS[pos].CodeEntite,"CONST")==0 && strcmp(TS[pos].ValConstan,"1")==0){
        return 1;
    }
    return 0;
}
//ajouter une bibliotheque
void AjouterLib(char Lib[]){
    if ( Recherche(Lib)==-1)
        {
            strcpy(TS[CP_TS].NomEntite,Lib); 
            strcpy(TS[CP_TS].CodeEntite,"LIB");
            CP_TS++;
        }
}

//le formatage des entres sorties
int VerfierInOut(char Chaine[]){
    int i= 0,index_ = 0;
    if(index_par>=0){
        while(i<strlen(Chaine)-1){
        if(Chaine[i]=='%'){
            if(((Chaine[i+1]=='s') || (Chaine[i+1]=='f') || (Chaine[i+1]=='d'))){
                if((Chaine[i+1]==VarOut[index_])){
                    index_++;
                }else{
                        memset( VarOut, '#', 100*sizeof(char));
                        index_par = -1;
                        return 0;    
                } 
            }
        }
        i++;
        }
    }else{return 0;}
    memset( VarOut, '#', 100*sizeof(char) );
    if((index_== 0 && index_ != index_par) || (index_<index_par+1)) {index_par = -1;return 0;}
    index_par = -1; 
    return 1;
}
// remplir le tableau varout 
void VarAffichage(char Var[]){
    int pos=Recherche(Var);
    if((strcmp(TS[pos].TypeEntite,"Entier")==0)) {index_par++;VarOut[index_par] = 'd';}
    if((strcmp(TS[pos].TypeEntite,"Reel"  )==0)) {index_par++;VarOut[index_par] = 'f';}
    if((strcmp(TS[pos].TypeEntite,"Chaine")==0)) {index_par++;VarOut[index_par] = 's';}
    
}

//_______________________________________affectation___________________________________________________


//recuperer le type de la l'idf
void setTypeVarAffct(char IDF[]){
    strcpy(TypeVarAffct,TS[Recherche(IDF)].TypeEntite); 
    
}

 
int verfierCompa(char TYPE[]){
    if(strcmp(TypeVarAffct,TYPE)!=0) return 0;
    return 1;
}

void AjouterOperand(char Oper[]){
    index_taboperand++;
    strcpy(tabOperand[index_taboperand],Oper);
}

void AjouterOperateur(char Oper){
    index_taboperateur++;
    tabOperateur[index_taboperateur]=Oper;
}

void AfficherTab(){
    int i;
    for(i = 0;i<=index_taboperand;i++){
        printf("%s \n",tabOperand[i]);
    }
}


int Verifier(){
    
    int i=0;
    
    for(i = 0;i<=index_taboperand;i++){
        if(strcmp(TypeVarAffct,"Reel")!=0){
            if(strcmp(TypeVarAffct,TS[Recherche(tabOperand[i])].TypeEntite)!=0){
                index_taboperateur=-1;
                index_taboperand=-1;
                return 0;
            }    
        }else{
            if(strcmp("Chaine",TS[Recherche(tabOperand[i])].TypeEntite)==0){ 
                index_taboperateur=-1;
                index_taboperand=-1;
                return 0;
            }    
        }
        
    } 
// typevaraffect = var;
    if(strcmp(TypeVarAffct,"Chaine")==0){
        for(i = 0;i<=index_taboperateur;i++){
            
            if(tabOperateur[i]!='+'){
                
                index_taboperateur=-1;
                index_taboperand=-1;
                return 0;
                }
        }
    }
    if(strcmp(TypeVarAffct,"Entier")==0){
        for(i = 0;i<=index_taboperateur;i++){
            if(tabOperateur[i]=='/'){
                index_taboperateur=-1;
                index_taboperand=-1;
                return 0;
                }
        }
    }

    

    return 1;
}


//incrementer le nbr ligne de commentaire
int Nbr_ligneCom(char comment[]){
    int cpt = 0,j,i = strlen(comment);
    for(j = 0;j<i;j++){
        if(comment[j]=='\n'){
            cpt++;
        }
    }
    return cpt;
}
