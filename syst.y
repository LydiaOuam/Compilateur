%token mc_import bib_io bib_lang pvg aco_frm aco_ouv <str>idf mc_class 
       mc_public <str>mc_entier mc_private mc_protected 
       <str>mc_reel <str>mc_chaine <str>idf_tab <entier>constint cro_ouv 
       cro_frm mc_const vrg egale chaine_const mc_valeur_reel
       bou_for arc_ouv arc_frm moin plus inf sup diff multi slach 
       in_instru output mc_main <str>commentaire guillemets


%{
int nb_ligne= 1;
char sauvType[20];

char souvop;
%}
%union {

int  entier;
char* str;
float reel;

}
%%
S: LISTE_BIB HEADER_CLASS {printf("syntaxe correcte !!");}
			
;
LISTE_BIB : BIB LISTE_BIB
          |
;		  
BIB: mc_import Nom_BIB pvg 
;	
Nom_BIB:bib_io
       |bib_lang
;

HEADER_CLASS: MODIFICATEUR mc_class idf aco_ouv CORPS aco_frm
;

MODIFICATEUR: mc_public | mc_private | mc_protected
;

CORPS: Partie_DEC_VAR Partie_Main    
;

Partie_Main: mc_main arc_ouv arc_frm aco_ouv Partie_INST aco_frm
;

Partie_INST : INST Partie_INST
             |
             ;

INST:  Boucle_for
       | Ecrire
       | Lecture
       | commentaire {      
                            //printf("%s \n",$1);      
                            nb_ligne = nb_ligne+Nbr_ligneCom($1);
                     }
       | Instru_affect    
;


Partie_DEC_VAR:  TYPE liste_idfs pvg Partie_DEC_VAR 
               | mc_const TYPE idf egale var pvg Partie_DEC_VAR { 
               													
                                                               if(doubleDeclaration($3)==0){AjouterConstEx($3,sauvType);}   
                                                               else{printf("%d: Double declaration  de %s a la ligne \n",nb_ligne,$3);}
                                                            
                                                                             
                                                               }
	           |mc_const TYPE idf pvg Partie_DEC_VAR { 
                                                               if(doubleDeclaration($3)==0){AjouterConstNex($3,sauvType);}   
                                                               else{printf("%d: Double declaration  de %s a la ligne \n",nb_ligne,$3);}
                                                                             
                                                               }
               |
;

liste_idfs : list_var | list_tab ;

var: constint| chaine_const | mc_valeur_reel; 

list_var: idf vrg list_var {
                            if(doubleDeclaration($1)==0)
                                     AjouterVar($1,sauvType);
                                     
                            else
		                     printf("%d: Double declaration  de %s a la ligne \n",nb_ligne,$1);
		              }
       | idf  {if(doubleDeclaration($1)==0)
                     AjouterVar($1,sauvType);
              else
		        printf("%d: Double declaration  de %s a la ligne \n",nb_ligne,$1);
		}
;

list_tab: idf_tab cro_ouv constint cro_frm vrg list_tab {
                                                               if(doubleDeclaration($1)==0){
                                                                      if($3<0){
                                                                             printf("taille du tableau doit etre plus de 0 \n");
                                                                      }
                                                                      else{
                                                                             AjouterTab($1,sauvType,$3);  
                                                                      }                               
                                                               }
                                                               else{
                                                                       printf("%d: Double declaration  de %s a la ligne \n",nb_ligne,$1);
                                                               }          
                                                        }
       | idf_tab cro_ouv constint cro_frm {
                                                 if(doubleDeclaration($1)==0){
                                                        if($3<0){
                                                               printf("taille du tableau doit etre plus de 0 \n");
                                                        }
                                                        else{
                                                               AjouterTab($1,sauvType,$3);  
                                                        }                               
                                                 }
                                                 else{
                                                         printf("%d: Double declaration  de %s a la ligne \n",nb_ligne,$1);
                                                 }
                                          }
;

TYPE:   mc_entier {strcpy(sauvType,$1);} 
       |mc_reel   {strcpy(sauvType,$1);}
       |mc_chaine {strcpy(sauvType,$1);}
;







//____________________________________________AFFECTATION_________________________________________________________________/

Instru_affect:  type_idf egale Liste_affect pvg  { 
                                                        if(Recherche("ISIL.lang")==-1){printf("Bibliotheque manquante! \n");}
                                                        else{if(Verifier()==0){printf("%d:Non compatibilite de type \n",nb_ligne);}} 
                                                 }
;

type_idf:       idf{
                           if(Recherche($1)==-1){printf("%d: IDF %s non declare \n",nb_ligne,$1);}
                           else{
                                   if(isConst($1)==1){printf("%d: impossible de changer le contenu d'une Const \n",nb_ligne);}
                                   else setvalConst($1);
                                   setTypeVarAffct($1);
                            }
                   }
              | idf_tab cro_ouv constint cro_frm {      
                                          if(VerfierDeppas($1,$3)==0){printf("%d:Depassemnt de taille de tableau \n",nb_ligne);}
                                          else{
                                                 if(Recherche($1)==-1){printf("%d: IDF %s non declare \n",nb_ligne,$1);}
                                                 setTypeVarAffct($1);
                                          }
                                               
                                   } 

Liste_affect:   var_affect Operation Liste_affect
              | var_affect
;

var_affect:     idf {if(Recherche($1)==-1){printf("%d: IDF %s non declare \n",nb_ligne,$1);} AjouterOperand($1);} 
              | idf_tab cro_ouv constint cro_frm {
                                                        if(VerfierDeppas($1,$3)==0){printf("%d:Depassemnt de taille de tableau \n",nb_ligne);}
                                                        if(Recherche($1)==-1){printf("%d: IDF %s non declare \n",nb_ligne,$1);}
                                                        else { AjouterOperand($1);}
                                                 }

              | chaine_const {if(verfierCompa("Chaine")==0){printf("%d:Non compatibilite de type \n",nb_ligne);}}
              | constint {if(verfierCompa("Chaine")!=0){printf("%d:Non compatibilite de type \n",nb_ligne);} 
                            if(souvop=='/' && $1==0){printf("%d:erreur semantique division par 0 \n",nb_ligne);} }
              |mc_valeur_reel {if(verfierCompa("Chaine")!=0){printf("%d:Incompatibilite de type\n",nb_ligne);} 
                            if(souvop=='/' && $1==0){printf("%d:erreur semantique division par 0 \n",nb_ligne);} }
;

Operation:  plus  {AjouterOperateur('+');}
          | moin  {AjouterOperateur('-');}
          | multi {AjouterOperateur('*');}
          | slach {AjouterOperateur('/'); souvop = '/';}
;



//__________________________________________________Boucle____________________________________________________

Boucle_for:  bou_for arc_ouv IntFor pvg Condition pvg Increment arc_frm aco_ouv Partie_INST aco_frm
             
;

IntFor:  idf egale constint {if(Recherche($1)==-1){printf("%d: IDF %s non declare \n",nb_ligne,$1);}}
       | idf egale idf {if(Recherche($1)==-1){printf("%d: IDF %s non declare \n",nb_ligne,$1);}}
       | idf egale idf_tab cro_ouv constint cro_frm {
                                                        if(Recherche($1)==-1){printf("%d: IDF %s non declare \n",nb_ligne,$1);}
                                                        if(VerfierDeppas($3,$5)==0){printf("%d:Depassemnt de taille  \n",nb_ligne);}
                                                        }
;

Compare: inf | sup | diff egale | egale inf | egale sup 
;

Condition: idf Compare idf {if((Recherche($1)==-1)&&(Recherche($3)==-1)){printf("%d: IDF %s non declare \n",nb_ligne,$1);}}
         | idf Compare constint {if(Recherche($1)==-1){printf("%d: IDF %s non declare \n",nb_ligne,$1);}}
         | constint Compare idf {if(Recherche($3)==-1){printf("%d: IDF %s non declare \n",nb_ligne,$1);}}                         
;

Increment: idf plus plus {if(Recherche($1)==-1){printf("%d: IDF %s non declare \n",nb_ligne,$1);}}
       | idf  moin moin {if(Recherche($1)==-1){printf("%d: IDF %s non declare \n",nb_ligne,$1);}}
;




//___________________________________________out & in_______________________________________________________/



Ecrire: output arc_ouv chaine_const liste_io arc_frm pvg {
                                                               if(Recherche("ISIL.io")==-1){printf("Bibliotheque manquante! \n");}
                                                               if(VerfierInOut($3)==0){printf("%d :Erreur format non respete de la fonction Out ligne \n",nb_ligne);}
                                                         }  
;

Lecture: in_instru arc_ouv chaine_const liste_io arc_frm pvg {
                                                                      if(Recherche("ISIL.io")==-1){printf("Bibliotheque manquante! \n");}
                                                                      if(VerfierInOut($3)==0){printf("%d :Erreur format non respete de la fonction In ligne  \n",nb_ligne);}
                                                              }
;


liste_io: vrg idf liste_io {if(Recherche($2)==-1){printf("%d: IDF %s non declare \n",nb_ligne,$2);}
                     else{VarAffichage($2);}
                   } 

       |  vrg idf_tab cro_ouv constint cro_frm   {if(Recherche($2)==-1){printf("%d: IDF %s non declare \n",nb_ligne,$2);}
                                                  else {if(VerfierDeppas($2,$4)==0){printf("%d:Depassemnt de taille  \n",nb_ligne);}  
                                                        else{VarAffichage($2);}}
                                                 } liste_io
       |  
;

%%

main()
{yyparse();
Affichage();
}
yywrap()
{}
yyerror(char*msg)
{
printf("erreur syntaxique a la ligne %d\n",nb_ligne);
}
